---
title: Drachenwald Academy of Defense 
excerpt: After the manner of the ancient schools of London
---
<img src="{{ site.baseurl }}{% link images/heraldry/academyofdefenseflag.gif %}" alt="Academy of Defense Badge">{: .align-left}  

The Academy promotes the noble art of fence within the Kingdom of Drachenwald through recognition, education, and training of fencers of all levels of skill. Membership is open, upon application to the Praeceptor, to all those who love the challenge of arms.  

Akin to a guild, it has ranks which candidates challenge into, by meeting three companions of the academy of appropriate skill level, in tests of skill at arms, and of endurance.  

* [Academy charter and bylaws]({{ site.baseurl }}{% link chartergroups/files/academy_defense_charter_2017_05_20.pdf %})  
* [Meeting minutes from Collegium of Defense in Mynydd Gwyn, May 2017]({{ site.baseurl }}{% link chartergroups/files/academy_meeting_minutes_06_05_2017.pdf %})  
* [Yahoogroup](http://groups.yahoo.com/group/DW_AC/)  
* [Facebook group](https://www.facebook.com/groups/DWAcademyOfDefense/)  

# The Academy 

## Companions: who support the Academy and its members through their service
Christian Trenchard  
Caitrin O'Sullivan  

## Members: who aspire to enter its ranks
Alfred Bale  
Anna Syveken  
Caitriona of the Ravens  
Constanza of Thamesreach  
Develyn Frost  
Ingvar Gråhök  
Jean-Oste de Murat  
Katarina Krognos af Ystad  
Konrad von Lewenstein  
Thomas Bainbridge of Manchester  

## Free Scholars: who have achieved the first rank of the Academy
Acarin St Cyr  
Áedán Páirce na Fia  
Aidan McGuire  
Alessandra Lucia della Robbia  
Alessandra di Riario d’Aretino  
Andreas Tillman von Severin  
Anna de Byxe  
Anna Laresdottir  
Aodhán Dhá Cheist   
Artemis Enright  
Barbara von Krempe  
Bella Donna of Flintheath  
Bernward von Eppstein  
Broder Magnus  
Caradoc de Tisbury  
Cecily Arderne  
Cedric of the Floppy Hat  
Chiara Danielli  
Cierian  
Culainn of Dun in Mara  
Cyfarwydd ferch Angharad  
Cynewulf of Wintancaestre  
Dorothea Pictora  
Duncan Reamhair  
Duncan the Shepherd  
Edward MacMillan  
Ekaterina Vasili  
Elizabeth Hollingsworth  
Elsa Magdalena  
Faolon Muirchu  
Faruk  
Fianna Rua Nic Mhathúna  
Gilbert Blackthorn  
Guillaume Jean-Pierre de Mortaine  
Guntrum von Wolkenstein  
Guy de Dinan  
Gyles Rochester  
Herr Sigvard Oelfuss  
Irg von der Ramsau  
Jacquelina de Bellmont  
James de Leon  
Jovi Torstensdottir  
Juliette Cavalieri  
Katharina  
Katherne Rischer  
Keina Vis  
Kyla O'Murphy  
Kytte of the Lake  
Lianna  
Lorenz de Wit   
Lorenzo Sando di Vincenzi  
Lucrezia Serafini da Roma  
Marcellina de Angelis  
Marguerite Dinard  
Marina Claudia Alessandra de Grado  
Martin von Vrankenvorde  
Maximillian Kern  
Megan  
Morna ní Conchubor  
Muirghein MacKiernan  
Nadiana Stjarnulf  
Nemet Arpad  
Nichole d'Audrieu  
Patrick of Vielburgen  
Rannulf Sweartfaxe  
Regina van Ameland  
Rhieinwen  
Richard the Rampant  
Robert D'Audrieu  
Ronja Ragnarsdottir von Rungholt  
Sadiq ibn Ahmad  
Seamus O'Neill  
Sigmundr Haakonarsson  
Simon  
Snakeborgh  
Stigot Eke av Haapsalu  
Sunniva Haraldsdatter  
Thomas Langland  
Thorngrimr Olafsson  
Tonis vom Ahrgebirge  
Umberto Lodovico Scolari  
Unegen of Eplaheimr  
Valdur of Gyllengran  
Wilhelm der Trunkenbold  
X-avier Daggerstone  
Yannick of Normandy  
Ysabella-Maria Vasquez de Grenada y Cortes  

## Provosts: who have achieved the middle rank of the Academy
Abel Grelsson  
Alexandre Lerot d'Avignon  
Agilmar von Sevelingen  
Armand d´Alsace  
Brian the Courteous  
Brynjolfr of Gyllengran  
Brynolf Haraldsson  
Chiudka the Odd  
Christopher FitzJohn  
Dafydd Gwynfardd  
Dafydd Myfygar  
Dubhgall MacÉibhearáird  
Dougald MacDonald  
Duncan Kerr  
Genevieve la Flechiere  
Gerhard von Wuestenburg  
Gilbert de Bracton  
Gottfried Kilianus  
Jasper Rose  
John de Barrie  
Kari Egilsson  
Karl Alrune  
Konrad von Schilberg  
Maredudd ap Gwylim  
Mikael Rantzow  
Nandolf von Biuira  
Nero Lupo  
Nicholas de Estleche dictus le Tardif  
Padraig Gliadrach O'Ceallaigh  
Pietari Uv  
Raphael d'Angelo  
Rebecca of Flintheath  
Richart von Brandenburg  
Risteárd Ruadh MacLeóid  
Sean  
Selinne de Nérac  
Therion Sean Storie  
Wolfram Burkhardt von Falkenstein  
Wolfram Flammenherz  

## Prefects: who hold the highest rank of the Academy
Æríkr inn Hárfagri  
Anna von Urwald  
Antonio di Rienzo Ruspoli  
Arenwald von Hagenburg  
Catlin le Mareschale  
Cernac the Navigator  
Domin d'Alsace  
Duarte Gonçalves  
Duncan Chaucer  
Esbiorn Jennson  
Etienne Fevre  
Fardäng Skvaldre  
Gwenllian verch Andras  
Leonet de Covenham  
Måns Knutsson Kotte  
Pól Ó Briain  
Tomas Pancaldo  
Vitus Polonius  

## Preceptor

Lord Esbiorn Jensson (D. Cordes) <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,100,97,118,105,100,46,99,111,114,100,101,115,64,103,109,97,105,108,46,99,111,109,39,62,100,97,118,105,100,46,99,111,114,100,101,115,64,103,109,97,105,108,46,99,111,109,60,47,97,62));</script>  

## Scribe
H Lady Gwenllian verch Andreas (K. Bernard) <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,107,97,116,104,108,121,110,98,101,114,110,97,114,100,64,103,109,97,105,108,46,99,111,109,39,62,107,97,116,104,108,121,110,98,101,114,110,97,114,100,64,103,109,97,105,108,46,99,111,109,60,47,97,62));</script>