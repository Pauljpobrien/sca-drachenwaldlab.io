---
title: Contributor guidelines
subtitle: Your newsletter needs you
featureimg: images/offices/chronicler/dtbanner.png
featurealt: Dragon's Tale Covers
---
<p>Contrary to popular belief, the Chronicler is not responsible for writing the Dragon's Tale. She (or he) mostly assembles each issue from the material contributed by the populace. Some material comes automatically - event announcements, officer missives etc, but for the rest, the Chronicler relies upon You - the reader.</p>
<p>The following section gives general guidelines for contributions.&nbsp; For more detailed information and technical stuff, see the section on detailed guidelines below.</p>
<h4>Type of Contributions</h4>
<p>Anything that would be of interest to SCA folk can be included in the newsletter.&nbsp; A recipe you recently redacted, an article on how to make a 14th century hat, a review of a book on medieval games, an article about your visit to a medieval mansion, a poem or song in the medieval style...</p>
<p>The newsletter also welcomes photographs and artwork.&nbsp; Each month, the front cover features a larger piece of art or a photograph, so there is a constant requirement for that.&nbsp; Sometimes, there is need for clip-art to illustrate articles.&nbsp; Often, there is a need for smaller pieces of art to separate two articles, fill the space at the end of a page and so on.</p>
<p>Contributions must be your own work and not contain material (other than justifiable quotes) from copyrighted material, unless you have proof of written permission from the copyright holder.&nbsp; See the section on copyright below for more details.</p>
<h4>Size of Contributions</h4>
<p>Articles are typically between one and six pages in length.&nbsp; A typical page in the <em>Dragon's Tale</em> is 800 words, which makes the article length 800-4800 words. Obviously, any illustrations or photographs will reduce the word count per page.</p>
<p>Drawings and illustrations come in various sizes.&nbsp; A stand-alone item could fill a whole page.&nbsp; Illlustrations for articles would typically be half a page or a quarter of a page.&nbsp; Cover art is typically around 2/3 of a page.</p>
<p>Clips and filler art will typically either be a column wide or a page wide, and come in various heights, as needed to fill the space at the end of an article, or to provide a spacer between articles.</p>
<h4>Format of Contributions</h4>
<p>Text for articles should be submitted electronically, where possible.&nbsp; Most word-processor formats can be accepted (preference for pre-2007 Word).&nbsp; If in doubt, save as Rich Text Format (RTF).&nbsp; Plain text can be taken, but be aware that accented characters don't always transfer well.</p>
<p>Photographs and scanned illustrations may be sent in most image formats - JPG, TIF, GIF etc.&nbsp; Send the highest resolution and highest quality you can.</p>
<p>Artwork and photographs should always be sent in as large size and high resolution as you can.&nbsp; They will be resized to fit when the issue is being edited.&nbsp; Sometimes, an illustration that is just the right size when you write the article in Word will be too small&nbsp; or too low a resolution to use in the final format of the newsletter.</p>
<p>For line drawings done electronically (e.g. with Corel Draw), please contact the Chronicler with details so we can determine the best format for submission.</p>
<p>While it is useful to have a version of the article fully formatted, with images in particular positions, please note that this formatting does not necessarily transfer well to the page format of the newsletter.&nbsp; Also, images embedded in a Word file are almost always impossible to extract in a useable format.&nbsp; Therefore, please always remember to send the article text in a plain Word file and the images as separate files in addition to the formatted version.</p>
<h4>Submitting Your Contribution</h4>
<p>Electronic submission is the preferred method.&nbsp; Most things can be submitted by email.&nbsp; If sending a contribution with a lot of high-resolution images, please contact the Chronicler first, as there is a size limitation on emails sent to the Chronicler address, and your own email system may have limitations on size.</p>
<p>Send to <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,99,104,114,111,110,105,99,108,101,114,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,39,62,99,104,114,111,110,105,99,108,101,114,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,60,47,97,62));</script> with "[DT] article title" in the Subject.</p>
<p><strong>Note</strong>: The chronicler will often put out requests to the various email lists requesting contributions.&nbsp; Do not send your contribution by replying to that email.&nbsp; The reason is that list emails and emails to the Chronicler get sent to different addresses and are filtered into different in-boxes.&nbsp; Items filtered to the email list address might not get seen among all the other list emails.</p>
<p>Hard copy of artwork may be sent by post to the address given on the main Chronicler page.&nbsp; Do not send the original as we cannot guarantee to be able to return it.</p>
<p>Hard copy of written articles will only be used in exceptional circumstances because of the effort involved in transcribing it to electronic form.</p>
<p>All original work must be accompanied by a written permission form.&nbsp; This can either be the <a href="http://www.drachenwald.sca.org/sites/default/files/permission.pdf" target="_blank">PDF form</a> (which needs to be printed, completed and either posted or scanned and emailed), or the <a href="http://www.drachenwald.sca.org/content/email-permission-form" target="_blank">email form</a> (which needs to be copied to your email, the blank sections completed and emailed).</p>
<h2>Detailed Guidelines</h2>
<p>These guidelines give more details about particular types of contribution</p>
<h3>Articles</h3>
<p>Articles that are mostly text are generally set in 10pt/11pt Garamond type, in a two-column layout.&nbsp; Where this would not suit the content (e.g. tabular data or lots of block quotations), then a single-column layout would be used.&nbsp; This would also apply to articles with illustrations.</p>
<p>A typical page in the Dragon's Tale works out around 800 words of continuous text.&nbsp; Non-continuous text, such as a list of ingredients, or a poem with short lines, can reduce that up to half.&nbsp;</p>
<p>A quarter-page illustration or photograph reduces the count by 200 words, a half-page illustration reduces it by 400 words.&nbsp; A "thumbnail" illustration, e.g. a picture of the cover of a book for a review tends to lose only 50 words.</p>
<p>Thus, an article with 3200 words and two half-page illustrations would come to five pages in the newsletter.</p>
<p>When submitting illustrated articles, bear in mind that the layout you submit is not necessarily the same as the way it will appear in the newsletter.&nbsp; Articles should therefore be written to reduce dependence on the relative positions of text and graphics.&nbsp; Refer to illustrations explicitly, rather than "in the diagram above/on the left" etc.&nbsp; Articles may be submitted with embedded graphics, but always send the graphics separately as the embedded versions do not transfer well to the final copy.&nbsp; Remember to give the graphics files meaningful names that relate to how they are referred to in the text, so call a file "Chancel Door.jpg" or "Figure3.jpg" rather than "IMG_1235.jpg"</p>
<p>Informative articles should cite sources where possible.&nbsp; There is no particular house style for citations, but they should suffice to identify the article and publication concerned - e.g.</p>
<p>HUTTON, Ronald, <em>The stations of the sun – A History of the Ritual Year in Britain, </em>Oxford &nbsp; 1996, p.134-145</p>
<p>When referencing Internet sources, try to reference the original sources (rather than Wikipedia).&nbsp; If the URL is long or complicated, use TinyURL or similar sites to generate a more "print-friendly" version.&nbsp; If this is done, then remember to name the site as well, e.g.</p>
<p>1) "<em>Magna Carta</em>" - Encyclopedia Britannica Online - <a href="http://tinyurl.com/yj7f6bj" title="http://tinyurl.com/yj7f6bj">http://tinyurl.com/yj7f6bj</a></p>
<h3>Graphics</h3>
<p>Graphics can be anything from photographs, scanned illustrations etc to simple line-drawn clip-art.&nbsp; There are three main areas these are used:</p>
<ul>
<li>Stand-alone pieces - photographs, artwork, comics &amp; cartoons and other graphics that are reproduced in their own right without an accompanying article or other text.&nbsp; This is most commonly&nbsp; used for the front cover. </li>
<li>Illustrations - any artwork, diagrams or photographs that accompany and illustrate an article.</li>
<li>Filler/decoration - clip-art, borders, scrolls, critters, heraldic images, badges, separators.&nbsp; Small pieces of artwork that can be used to decorate an article, provide end of page or ed of column fillers, separate one article from another, provide a banner heading etc.</li>
</ul>
<h4>Stand-Alone Graphics - Cover Art</h4>
<p>The main use for stand-alone art is for the front cover of the magazine.&nbsp; A new image is needed each month.&nbsp; The cover art can be a photograph or a piece of original artwork.</p>
<p>The space available is 135mm high by a maximum of 130mm wide.&nbsp; Images may be narrower in proportion as this fits well with the arrangement of white space on the cover.</p>
<p>Photographs should be in vertical ("portrait") format rather than horizontal ("landscape"), as this best fits the space available on the cover.</p>
<p>Drawings and artwork should likewise also be taller than they are wider to make best use of the space available.&nbsp; In terms of appearance, drawings etc work best if:</p>
<ul>
<li>They have a rectangular border, or the shape of the drawing clearly indicate a rectangular bounding box. An L-shape is OK provided the lines are such that they suggest an opposite corner.</li>
<li>Have a partial bounding box, left, right and below, with the upper sides and top free space. Left &amp; right sides should both have a reasonable proportion of the height and not be too different in height (e.g. full height one side and only 5mm or none on the other).</li>
<li>Have only a bottom boundary, with sides and top left free space.</li>
<li>Have no obvious cutoff bounding box.</li>
</ul>
<h4>Images and Photographs - General Considerations</h4>
<p>Images are reproduced in half-tone black &amp; white.&nbsp; Images should therefore not have too much fine detail, or areas of low contrast, as these will not reproduce well.</p>
<p>Photographs should obviously be of a relevant subject - medieval building, photographs taken at events and such like.&nbsp; Where the photograph features the faces of individuals such that they can be easily recognised, be sure that you have the permission of the people concerned, especially where children are concerned.</p>
