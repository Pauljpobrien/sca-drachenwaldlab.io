---
title: Integrating old and new
---
<p>Retaining new members is an important job of the chatelain; it may be the<br />
        most difficult job, too. The key to success is to get the new people<br />
        involved, and to make them feel welcome. Try to offer them something that<br />
        can not be found elsewhere. There are many ways that this may be done. Here<br />
        are a few suggestions:</p>
<ol>
<li>Have a new members orientation meeting (after recruiting) to explain<br />
          the SCA and to let people get acquainted. See Appendix II for a sample<br />
          orientation meeting outline.</li>
<li>Match each new person with an experienced person for a couple of<br />
          months. The experienced person can answer questions, help the new person<br />
          get ready for an event, introduce the new person to others, etc. There can<br />
          even be a gradutation ceremony at the end of the mentoring<br />
          relationship.</li>
<li>Give new people a New Members Packet. (Every group should organise one<br />
          appropriate for themselves.) This relieves the pressure on experienced<br />
          members to try to explain everything, as well as pressure on the new<br />
          person who does not know what questions to ask. The New Members Packet is<br />
          explained in the previous section.</li>
<li>Have activities that help the new people and the old people interact.<br />
          Some suggestions of this sort include: a garb making session, a cooking<br />
          session and feast, a class on calligraphy or any hands-on activity.</li>
<li>Have a "How-to-go-to-an-event" meeting before a nearby event. Explain<br />
          what to take to an event, what not to take, what to expect, and how to<br />
          act. (It is never too early to tell people that they are expected to<br />
          maintain SCA standards of courteous and responsible behaviour.) Help them<br />
          find lifts or car pools, (or at least maps), feast gear, garb, etc.</li>
<li>Get the new member to an event. The events are what make the SCA<br />
          special, not the local meetings and fighter practices.</li>
<li>Invite the new members to do non-SCA things with local SCA members,<br />
          such as a video party, a pizza party, or bowling or just going out for a<br />
          drink. These informal gatherings can be reassuring to the newcomers,<br />
          because they are more confident of what is expected of them.</li>
<li>Do not expect everyone to have the same SCA interests. College age<br />
          members will often have different interests than the older members.<br />
          Respect those differences and let people be themselves.</li>
<li>Throw a mini-event for the new people. This provides a place where new<br />
          peole can learn what an event is like, without as much pressure.<br />
          Established members often have more time to help out newcomers at a<br />
          mini-event.</li>
<li>Give new people a job, get them involved. Include the new people in<br />
          what ever is happening in your group. They can help with the pre-cook for<br />
          a feast (been there and got me hooked!), help do the layout for the<br />
          newsletter, etc. (I do not recommend, however, giving brand-new members an<br />
          office.)</li>
<li>If the newcomers are a family, do not forget the kids. Have activities<br />
          that interest everyone. Why not contact the local Children's Guild or the<br />
          Kingdom Guild Head.</li>
<li>Find established members (perhaps from other groups) who have<br />
          interests the same as your new people, and introduce them to each other.<br />
          For example, if the new person is interested in fighting, introduce the<br />
          new person to a friendly knight or fighting instructor at an event.</li>
</ol>